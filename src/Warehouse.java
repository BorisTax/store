

import java.io.*;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Scanner;
import java.lang.*;

public class Warehouse {
    private ArrayList<Goods> goodsList = new ArrayList<Goods>();
    private static Warehouse instance;
    private static final String DATABASE_NAME="assets/goods.csv";
    private boolean goodsLoaded;
    private Warehouse() {
        goodsLoaded=loadGoods();
    }

    public static Warehouse getInstance() {
        if (instance == null) {
            instance = new Warehouse();
        }
        return instance;

    }
    public boolean isGoodsLoaded(){return goodsLoaded;}

    public ArrayList<Goods> getGoodsList() {
        return goodsList;
    }

    public void saveGoods(){
        DecimalFormat df = new DecimalFormat("#.00");
        DecimalFormatSymbols dfc=new DecimalFormatSymbols();
        dfc.setDecimalSeparator('.');
        df.setDecimalFormatSymbols(dfc);
        try {
            FileWriter writer = new FileWriter("assets/goods.csv", false);
            for(Goods goods:goodsList){
                String line=goods.getName()+";"+df.format(goods.getPurchasePrice())+";"+goods.getClassification()+
                        ";"+goods.getVolume()+";"+goods.getComp_strength()+";"+
                        goods.getAvailability()+"\n";
                writer.write(line);
            }
            writer.flush();
            System.out.println("Goods database updated");
        }catch (IOException e){
            System.out.println(e.getMessage());
        }
    }

    private boolean loadGoods() {
        BufferedReader reader;
        try {
            reader = new BufferedReader(new FileReader(DATABASE_NAME));
        } catch (FileNotFoundException e) {
            System.out.println("Goods database doesn't exist");
            return false;
        }
        String line;
        Scanner scanner;
        int id = 0;
        try {
            while ((line = reader.readLine()) != null) {
                scanner = new Scanner(line);
                Goods goods = new Goods();
                goods.setId(id);
                scanner.useDelimiter(";");
                int index = 0;

                while (scanner.hasNext()) {

                    String data = scanner.next();
                    switch (index) {
                        case 0:
                            goods.setName(data);
                            break;
                        case 1:
                            goods.setPurchasePrice(Double.parseDouble(data));
                            break;
                        case 2:
                            goods.setClassification(data);
                            break;
                        case 3:
                            goods.setVolume(data);
                            break;
                        case 4:
                            goods.setComp_strength(data);
                            break;
                        case 5:
                            goods.setAvailability(Integer.parseInt(data));
                            break;
                    }
                    index++;
                }
                id++;

                goodsList.add(goods);
            }
        } catch (Exception e) {
            System.out.printf("Goods database reading error on line %d\n", id);
            return false;
        } finally {
            try {
                reader.close();
            } catch (IOException e) {
                System.out.println(e.getMessage());
            }

        }
        return true;
    }
}
