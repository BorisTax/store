import java.util.ArrayList;
import java.util.Random;

class Order{
    int id;
    int count;
    public Order(int id, int count){
        this.id=id;
        this.count=count;
    }
}

public class Customer {
    private final int maxGoodsBuyCount=10; // maximum goods count of each goods type that customer can buy
    private int maxGoodsTypeCount;//will depend of goods count in database
    private int goodsTypeCount;//maximum goods type count that customer want to buy
    private ArrayList<Order> ordersList=new ArrayList<Order>();
    public Customer(int maxGoodsTypeCount){
        this.maxGoodsTypeCount=maxGoodsTypeCount;
        makeOrder();
    }
    private void makeOrder(){
        RealRandom random=new RealRandom();
        Random rand=new Random();
        goodsTypeCount=random.nextInt(0,maxGoodsTypeCount);
        ordersList.clear();
        boolean[] checkUniqueId=new boolean[maxGoodsTypeCount];
        for(int i=0;i<goodsTypeCount;i++){
            int id;
            do { //this section generates unique id
                id=Math.abs(rand.nextInt())%maxGoodsTypeCount;
            }while(checkUniqueId[id]);
            checkUniqueId[id]=true;
            int count=random.nextInt(1,maxGoodsBuyCount);
            ordersList.add(new Order(id,count));

        }
    }
    public ArrayList<Order> getOrdersList(){
        return ordersList;
    }

}
