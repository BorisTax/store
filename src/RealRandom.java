import java.util.Random;

public class RealRandom {

    private Random random;

    public RealRandom(){
        random=new Random();
    }
    public int nextInt(int begin, int end){
        int range=end-begin;
        return range-(int)Math.round(Math.sqrt(Math.abs(random.nextLong())%(range*range)))+begin;
    }
}
