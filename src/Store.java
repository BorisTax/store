import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;

public class Store {
    private static final int STANDART_MARKUP = 10;//percent
    private static final int WEEKEND_MARKUP = 15;//percent
    private static final int EVENING_MARKUP = 8;//percent
    private static final int TWO_MORE_UNITS_MARKUP = 7;//percent
    private static final String[] dayOfWeek={"Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"};
    private final int beginWorkingHour=8;
    private final int endWorkingHour=21;
    private final int beginEveningHour=18;
    private final int endEveningHour=20;
    private final int maxCustomersPerDay=10;
    private int goodsTypesCount;
    Warehouse warehouse;
    ArrayList<Goods> goodsList;
    private int hour;
    private int days;
    private int daysPerMonth;
    private boolean expired=false;
    private int[] totalGoodsSold;
    private double[] totalSum;
    private int[] totalGoodsPurchased;
    private double[] totalSumPurchase;

    public Store(int period) {
        warehouse = Warehouse.getInstance();
        if(!warehouse.isGoodsLoaded()) {
            System.out.println("Goods database wasn't loaded");
            expired=true;
            return;
            }
        goodsList = warehouse.getGoodsList();
        goodsTypesCount=goodsList.size();
        daysPerMonth = period;
        hour=8;
        days=1;
        totalGoodsPurchased=new int[goodsTypesCount];
        totalGoodsSold=new int[goodsTypesCount];
        totalSum=new double[goodsTypesCount];
        totalSumPurchase=new double[goodsTypesCount];
    }

    public boolean isExpired(){
        return expired;
    }

    public void next() {
        if(!warehouse.isGoodsLoaded()) {
            System.out.println("Goods database wasn't loaded");
            return;
            }
        if(hour==beginWorkingHour) {
            System.out.println();
            System.out.printf("Day %d %s\n", days, dayOfWeek[days % 7]);
            }

        System.out.println();
        System.out.printf("Current time: %d:00  Day %d %s\n", hour,days,dayOfWeek[days%7]);
        if (hour==endWorkingHour){
            endOfDay();
            return;
            }
        workHour();
        hour++;
    }

    private void workHour() {
        markupRefresh();
        RealRandom random=new RealRandom();
        DecimalFormat df = new DecimalFormat("#.00");
        int customersCount=random.nextInt(1,maxCustomersPerDay);
        System.out.printf("%d customers comes.\n",customersCount);
        for(int customerIndex=1;customerIndex<=customersCount;customerIndex++){
            Customer customer=new Customer(goodsTypesCount);
            System.out.printf("Customer №%d buys:\n",customerIndex);
            ArrayList<Order> ordersList=customer.getOrdersList();
            double totalSumForCustomer=0;
            for(Order order:ordersList){
                String name = goodsList.get(order.id).getName();
                int count = Math.min(goodsList.get(order.id).getAvailability(), order.count);
                int markup;
                double price;
               if(count>0) {
                   if (count >= 2) goodsList.get(order.id).setMarkUp(TWO_MORE_UNITS_MARKUP);
                   markup = goodsList.get(order.id).getMarkUp();
                   price = goodsList.get(order.id).getPurchasePrice() * (100 + markup) / 100;
                   totalGoodsSold[order.id] += count;
                   totalSum[order.id]+=price * count;
                   totalSumForCustomer += price * count;
                   goodsList.get(order.id).setAvailability(goodsList.get(order.id).getAvailability()-count);
                   if (totalSumForCustomer != 0)
                       System.out.printf("    %s - %d pieces at %s per piece with a mark-up of %d%c \n", name, count, df.format(price), markup, '%');

               }
               goodsList.get(order.id).setMarkUp(STANDART_MARKUP);
            }
            if(totalSumForCustomer==0) System.out.println("    nothing\n");else {
                System.out.println("    Total: "+df.format(totalSumForCustomer));
                System.out.println();
                }
        }

    }

    private void endOfDay(){
        System.out.printf("End of the day %d \n",days);
        for(Goods goods:goodsList){
            if (goods.getAvailability()<10){
                goods.setAvailability(goods.getAvailability() + 150);
                System.out.printf("%s - 150 pieces were purchased \n", goods.getName());
                totalGoodsPurchased[goods.getId()]+=150;
                totalSumPurchase[goods.getId()]+=150*goods.getPurchasePrice();

            }
            goods.setMarkUp(STANDART_MARKUP);
        }
        hour=8;
        days++;
        System.out.println();
        if (days>daysPerMonth) {
            expired = true;
            System.out.println();
            System.out.println("End of the month");
            makeReport();
            warehouse.saveGoods();
            }
    }
    private void makeReport(){
        DecimalFormat df = new DecimalFormat("#.00");
        try {
            FileWriter writer = new FileWriter("assets/report.txt", false);
            writer.write("Sold:\n");
            for (int i = 0; i < goodsTypesCount; i++) {
                writer.write(goodsList.get(i).getName() + " - " + totalGoodsSold[i] + " pieces\n");
            }
            writer.write("\n");
            writer.write("Purchased:\n");
            for (int i = 0; i < goodsTypesCount; i++) {
                writer.write(goodsList.get(i).getName() + " - " + totalGoodsPurchased[i] + " pieces\n");
            }
            writer.write("\n");
            writer.write("Profit = Produced - Outlay:\n");
            double totalProfit = 0;
            double totalProduce=0;
            double totalOutlay = 0;
            for (int i = 0; i < goodsTypesCount; i++) {
                double produce=totalSum[i];
                double outlay = totalSumPurchase[i];
                double profit = produce - outlay;
                writer.write(goodsList.get(i).getName() + " : " + df.format(profit)+" = "+df.format(produce)+" - "+df.format(outlay)+"\n");
                totalProfit += profit;
                totalProduce+=produce;
                totalOutlay+=outlay;
            }
            writer.write("Total: " + df.format(totalProfit)+" = "+df.format(totalProduce)+" - "+df.format(totalOutlay)+"\n");
            writer.write("\n");
            writer.flush();
            System.out.println("Report saved at report.txt");
        }catch (IOException e){
            System.out.println(e.getMessage());
        }
    }
    private void markupRefresh(){
        for(Goods goods:goodsList){
            if(goods.getMarkUp()!=TWO_MORE_UNITS_MARKUP){
                goods.setMarkUp(STANDART_MARKUP);
                if(dayOfWeek[days%7].equals("Saturday")||dayOfWeek[days%7].equals("Sunday")) goods.setMarkUp(WEEKEND_MARKUP);
                if((hour>=beginEveningHour)&&(hour<endEveningHour))goods.setMarkUp(EVENING_MARKUP);

            }
        }
    }


}
